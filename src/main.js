import Vue from 'vue';
import router from './router/router';
import App from './App.vue';

Vue.config.productionTip = false;

console.log(router);

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
