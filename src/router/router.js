import Vue from 'vue';
import VueRouter from 'vue-router';
import MealPage from '../views/MealPage.vue';
import HomePage from '../views/HomePage.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomePage,
  },
  {
    path: '/meal/:id',
    name: 'MealPage',
    component: MealPage,
    props: true,
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
